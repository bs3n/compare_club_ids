﻿using CompareClubIDs.Domain.Models;
using CompareClubIDs.Domain.Services;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace CompareClubIDs.Data
{
    public class UIDsFileService : FileService<UIDsFileRow>
    {
        private readonly List<UIDsFileRow> _rows = new List<UIDsFileRow>();

        override public IEnumerable<UIDsFileRow> GetRows()
        {
            return _rows;
        }

        override public void ReadRows(string source)
        {
            using StreamReader sr = new StreamReader(source, Encoding ??= GetEncoding(source));

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                string[] fields = line.Split(Seperator ??= GetSeperator(source));

                _rows.Add(new UIDsFileRow()
                {
                    FootballManagerID = fields[5],
                    FIFAManagerID = fields[6].Replace("0x", string.Empty),
                    RawString = line
                });
            }
        }

        public void RemoveRow(UIDsFileRow row)
        {
            _rows.Remove(row);
        }
    }
}