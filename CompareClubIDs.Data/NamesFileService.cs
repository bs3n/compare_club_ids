﻿using CompareClubIDs.Domain.Models;
using CompareClubIDs.Domain.Services;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace CompareClubIDs.Data
{
    public class NamesFileService : FileService<NamesFileRow>
    {
        private readonly List<NamesFileRow> _rows = new List<NamesFileRow>();

        override public IEnumerable<NamesFileRow> GetRows()
        {
            return _rows;
        }

        override public void ReadRows(string source)
        {
            using StreamReader sr = new StreamReader(source, Encoding ??= GetEncoding(source));
            int index = 0;

            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                List<string> columns = line.Split(Seperator ??= GetSeperator(source)).ToList();

                _rows.Add(new NamesFileRow()
                {
                    FootballManagerID = columns.ElementAt(7),
                    LineIndex = index++,
                    Columns = columns
                });
            }
        }
    }
}