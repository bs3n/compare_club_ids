﻿using CompareClubIDs.Domain.Models;
using CompareClubIDs.Domain.Services;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace CompareClubIDs.Data
{
    public class CountryDataFileService : FileService<CountryDataFileEntry>
    {
        private readonly List<CountryDataFileEntry> _rows = new List<CountryDataFileEntry>();

        override public IEnumerable<CountryDataFileEntry> GetRows()
        {
            return _rows;
        }

        override public void ReadRows(string source)
        {
            foreach (string filename in Directory.EnumerateFiles(source, "CountryData*.sav"))
            {
                string[] lines = File.ReadAllLines(filename, Encoding ??= GetEncoding(filename));

                for (int i = 0; i < lines.Length; i++)
                {
                    if(Regex.IsMatch(lines[i], "%INDEX%CLUB\\d"))
                    {
                        _rows.Add(new CountryDataFileEntry()
                        {
                            Name = lines[i + 6].Split(",")[0].Replace("\"", string.Empty),
                            ShortName = lines[i + 8].Split(",")[0].Replace("\"", string.Empty),
                            Abbreviation = lines[i + 10].Split(",")[0].Replace("\"", string.Empty),
                            FIFAManagerID = int.Parse(lines[i + 4]).ToString("X8")
                        });
                    }
                }
            }
        }
    }
}