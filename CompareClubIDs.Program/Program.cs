﻿using CompareClubIDs.Data;
using CompareClubIDs.Domain.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CompareClubIDs.Program
{
    class Program
    {
        private static UIDsFileService uIDsFileService = new UIDsFileService();
        private static NamesFileService namesFileService = new NamesFileService();
        private static CountryDataFileService countryDataFileService = new CountryDataFileService();

        [STAThread]
        static void Main(string[] args)
        {
            Dictionary<string, string> filenames = ReadFiles();

            WriteToConsole("Verarbeite UIDs ...");
            AppendUIDtoNamesFile();

            WriteToConsole("Verarbeite Vereinsnamen ...");
            AppendClubNamestoNamesFile();

            WriteNewNamesFile(filenames["Names"]);

            WriteLeftoverRowsFile(filenames["UIDs"]);

            Console.WriteLine();
            WriteToConsole("Fertig. Beliebige Taste drücken um das Fenster zu schließen.");
            Console.ReadKey(true);
            Application.Exit();
        }

        private static void WriteToConsole(string text)
        {
            Console.WriteLine(text);
            Console.WriteLine();
        }

        private static Dictionary<string, string> ReadFiles()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog() { Multiselect = false, RestoreDirectory = true };
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog() { ShowNewFolderButton = false, UseDescriptionForTitle = true };

            Dictionary<string, string> filenames = new Dictionary<string, string>();

            openFileDialog.Title = "UID Datei auswählen";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                WriteToConsole("UID Datei wird eingelesen ...");
                uIDsFileService.ReadRows(openFileDialog.FileName);
                filenames.Add("UIDs", openFileDialog.FileName);
            }
            else
                Application.Exit();

            openFileDialog.Title = "Names Datei auswählen";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                WriteToConsole("Names Datei wird eingelesen ...");
                namesFileService.ReadRows(openFileDialog.FileName);
                filenames.Add("Names", openFileDialog.FileName);
            }
            else
                Application.Exit();

            folderBrowserDialog.Description = "CountryData Ordner auswählen";
            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                WriteToConsole("CountryData Dateien werden eingelesen ...");
                countryDataFileService.ReadRows(folderBrowserDialog.SelectedPath);
            }
            else
                Application.Exit();

            return filenames;
        }

        private static void AppendUIDtoNamesFile()
        {
            foreach (NamesFileRow namesFileRow in namesFileService.GetRows())
            {
                UIDsFileRow uIDsFileRow = uIDsFileService.GetRows().FirstOrDefault(c => c.FootballManagerID == namesFileRow.FootballManagerID);

                if (uIDsFileRow != null)
                {
                    namesFileRow.FIFAManagerID = uIDsFileRow.FIFAManagerID;
                    namesFileRow.Columns.Add(uIDsFileRow.FIFAManagerID);

                    uIDsFileService.RemoveRow(uIDsFileRow);
                }
            }
        }

        private static void AppendClubNamestoNamesFile()
        {
            foreach (NamesFileRow namesFileRow in namesFileService.GetRows())
            {
                CountryDataFileEntry countryDataFileEntry = countryDataFileService.GetRows().FirstOrDefault(r => r.FIFAManagerID.Equals(namesFileRow.FIFAManagerID));

                if (countryDataFileEntry != null)
                {
                    namesFileRow.Columns[11] = countryDataFileEntry.Name;
                    namesFileRow.Columns[12] = countryDataFileEntry.ShortName;
                    namesFileRow.Columns[13] = countryDataFileEntry.Abbreviation;
                }
            }
        }

        private static void WriteNewNamesFile(string filename)
        {
            IOrderedEnumerable<NamesFileRow> newNamesFileRows = namesFileService.GetRows().OrderBy(c => c.LineIndex);

            filename = AddSuffixToFilename(filename, "_new");
            WriteToConsole(string.Format("Schreibe Datei {0} ...", filename));

            using StreamWriter sw = new StreamWriter(filename, false, namesFileService.Encoding);
            foreach (NamesFileRow newNamesFileRow in newNamesFileRows)
            {
                sw.WriteLine(string.Join(namesFileService.Seperator, newNamesFileRow.Columns));
            }
        }

        private static string AddSuffixToFilename(string filename, string suffix)
        {
            string path = Path.GetDirectoryName(filename);
            filename = Path.GetFileNameWithoutExtension(filename) + suffix + Path.GetExtension(filename);
            return Path.Combine(path, filename);
        }

        private static void WriteLeftoverRowsFile(string filename)
        {
            filename = AddSuffixToFilename(filename, "_leftover");
            WriteToConsole(string.Format("Schreibe Datei {0} ...", filename));

            using StreamWriter sw = new StreamWriter(filename, false, uIDsFileService.Encoding);
            foreach (UIDsFileRow uIDsFileRow in uIDsFileService.GetRows())
            {
                sw.WriteLine(uIDsFileRow.RawString);
            }
        }
    }
}