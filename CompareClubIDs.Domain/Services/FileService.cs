﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace CompareClubIDs.Domain.Services
{
    public abstract class FileService<T> : IFileService<T>
    {
        public string Seperator { get; set; }
        public Encoding Encoding { get; set; }

        protected string GetSeperator(string filename)
        {
            string[] seperators = new string[] { ",", ";", "\t" };

            using StreamReader sr = new StreamReader(filename, Encoding ??= GetEncoding(filename));
            string firstLine = sr.ReadLine();

            int maxCount = 0;
            string seperator = string.Empty;
            foreach (string item in seperators)
            {
                int count = Regex.Matches(firstLine, item).Count;
                if (count > maxCount)
                {
                    maxCount = count;
                    seperator = item;
                }
            }

            Seperator = seperator;
            return seperator;
        }

        protected Encoding GetEncoding(string filename)
        {
            using StreamReader sr = new StreamReader(filename);
            sr.Peek();
            Encoding = sr.CurrentEncoding ?? Encoding.Default;

            return Encoding;
        }

        public abstract IEnumerable<T> GetRows();

        public abstract void ReadRows(string source);
    }
}