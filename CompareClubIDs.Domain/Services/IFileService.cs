﻿using CompareClubIDs.Domain.Models;
using System.Collections.Generic;

namespace CompareClubIDs.Domain.Services
{
    public interface IFileService<T>
    {
        public IEnumerable<T> GetRows();
        public void ReadRows(string source);
    }
}