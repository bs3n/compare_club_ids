﻿namespace CompareClubIDs.Domain.Models
{
    public class UIDsFileRow
    {
        public string FootballManagerID { get; set; }
        public string FIFAManagerID { get; set; }
        public string RawString { get; set; }
    }
}