﻿using System.Collections.Generic;

namespace CompareClubIDs.Domain.Models
{
    public class NamesFileRow
    {
        public string FootballManagerID { get; set; }
        public string FIFAManagerID { get; set; }
        public int LineIndex { get; set; }
        public List<string> Columns { get; set; }
    }
}