﻿namespace CompareClubIDs.Domain.Models
{
    public class CountryDataFileEntry
    {
        public string FIFAManagerID { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Abbreviation { get; set; }
    }
}